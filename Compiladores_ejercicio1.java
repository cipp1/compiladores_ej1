package compiladores_ejercicio1;
import java.util.Scanner;


public class Compiladores_ejercicio1 {

    
    public static void main(String[] args) {
        
        Scanner s = new Scanner(System.in);
        String frasenueva = "";
        
        System.out.println("Ingrese su texto.");
        String frase = s.nextLine();
        
        
        for(int i=0;i<frase.length();i++){
            switch(frase.charAt(i)){
                case 'i':
                    frasenueva += "wi";
                break;
                case 'e':
                    frasenueva += "we";
                break;
                default:
                    frasenueva += frase.charAt(i);
            }
        }
        
        frasenueva += " owo";
        
        System.out.println("Su frase cambiada:\n"+frasenueva);
        
    }
    
}